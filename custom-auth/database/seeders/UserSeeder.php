<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Hash;
use DB;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //cmd)php artisan db:seed --class=UserSeeder
        DB::table('users')->insert([
            "name"=>"apurv",
            "email"=>"apurv@gmail.com",
            "password"=>\Hash::make('apurv@123'),
     ]);
    }
}
