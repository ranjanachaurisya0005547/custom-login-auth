<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use Str;

class LoginController extends Controller
{
    public function userLogin(Request $request){
        try{
            $rules=[
                'email' => 'required|string',
                'password' => 'required|string'
            ];

            $validator=Validator::make($request->all(),$rules);

            if($validator->fails()){
                 return response()->json(
                     [
                         'errors'=>$validator->errors(),
                         'success'=>false,
                         'status'=>201
                     ]
                 );
            }else{ 
              $email=$request->input('email');
              $user=User::where('email',$email)->first();
              if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
                  $token=Str::random(100);
                  return response()->json([
                      'message'=>'Login Successfully !',
                      'success'=>true,
                      'accessToken'=>$token,
                      'code'=>200,
                  ]);             
              
            }else{
                return response()->json([
                    'message'=>'Email or Password Invailid !',
                    'success'=>false,
                    'code'=>201,
                ]);
            }
        }
        }catch(\Exception $ex){
            throw new \Exception($ex->getMessage());
        }
    }


    //Admin Dashboard
    public function adminDashboard(){
        return "Welcome to the Admin Dashboard";
    }

    //Get All registered User
    public function showUser(){
        $user=User::all();
        return $user;
    }
}
