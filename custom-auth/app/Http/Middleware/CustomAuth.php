<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CustomAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token=$request->headers->get('Authorization');
        
        if(empty($token)){
              return response()->json([
                  'message'=>'Unauthorized Access !',
                  'success'=>false,
                  'code'=>400,
              ]);
        }

        return $next($request);
    }
}
