<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/custom')->group(function(){
    Route::post('/login',[LoginController::class,'userLogin'])->name('user.login');

    Route::middleware('custom_auth')->group(function(){
       Route::get('/dashboard',[LoginController::class,'adminDashboard'])->name('admin.dashboard');
       Route::get('/registered-users',[LoginController::class,'showUser'])->name('admin.showusers');
    });
 });
